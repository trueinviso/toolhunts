class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.boolean :design_list, :default => false
      t.boolean :photography_list, :default => false
      t.boolean :marketing_list, :default => false
      t.boolean :development_list, :default => false
      t.timestamps
    end
  end
end
