# Be sure to restart your server when you modify this file.

# The MailChimp API key used by Gibbon and Mailchimp.
require 'MailChimpManager'

ENV['MC_API_KEY'] || raise('No mailchimp api key specified.  *sad face')
ENV['MC_LIST_ID'] || raise('No mailchimp list id specified.  *single tear')