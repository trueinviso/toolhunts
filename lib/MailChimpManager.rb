class MailChimpManager
  @@already_subscribed_code = '214'

  def subscribe(user)
    ap user
    # Add user to the correct lists
    add_member_to_list(user, "0b4c869a0b") if user.design_list == true
    add_member_to_list(user, "a5c09d0a96") if user.photography_list
    add_member_to_list(user, "e2483882fc") if user.marketing_list
    add_member_to_list(user, "5d3fdcf866") if user.development_list

  end

  def add_member_to_list(user, list_id)
    g = Gibbon::Request.new
    begin
      response = g.lists(list_id).members.create(body: {email_address: user.email, status: "subscribed"})

      Rails.logger.debug "Mailchimp subscription response: #{response}"

      if response != true && response['code'] == @@already_subscribed_code
        Rails.logger.info "duplicate subscription: updating member list"
        g.listUpdateMember(
            :id => list_id,
            :email_address => user.email,
            :merge_vars => merge_vars
        )
      end
    rescue
      Rails.logger.error "Mailchimp API failed trying to add #{user.email} to list #{list_id}"
    end
  end
end