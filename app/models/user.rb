class User < ActiveRecord::Base
  def self.subscribe_user(params)
    #check for user
    count = User.count_by_sql("SELECT COUNT(*) FROM users WHERE email = '"+params[:email]+"'")

    if count > 0
      user = update_user(params[:email], params[:design], params[:photography], params[:development], params[:marketing])
    else
      user = create_new_user(params[:email], params[:design], params[:photography], params[:development], params[:marketing])
    end

    add_user_to_mailchimp(user)
  end

  def self.create_new_user(email, design, photography, development, marketing)
    user = User.new
    user.design_list = design
    user.photography_list = photography
    user.development_list = development
    user.marketing_list = marketing
    user.email = email
    user.save!
    user
  end

  def self.update_user(email, design, photography, development, marketing)
    user = User.find_by_email(email)
    user.design_list = design
    user.photography_list = photography
    user.development_list = development
    user.marketing_list = marketing
    user.save!
    user
  end

  def self.add_user_to_mailchimp(user)
    m = MailChimpManager.new
    #Error trap the Mailchimp API call, we don't want the user create to fail but we want to log error
    m.subscribe(user)
  end

  # == Schema Information
  #
  # Table name: users
  #
  #  id                        :integer          not null, primary key
  #  email                     :string
  #  design_list               :boolean          default(false)
  #  photography_list          :boolean          default(false)
  #  marketing_list            :boolean          default(false)
  #  development_list          :boolean          default(false)
  #  created_at                :datetime         not null
  #  updated_at                :datetime         not null
end
