# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
emailPattern = /// ^ #begin of line
   ([\w.-]+)         #one or more letters, numbers, _ . or -
   @                 #followed by an @ sign
   ([\w.-]+)         #then one or more letters, numbers, _ . or -
   \.                #followed by a period
   ([a-zA-Z.]{2,6})  #followed by 2 to 6 letters or periods
   $ ///i            #end of line and ignore case

$ ->
  $(document).mouseup (e) ->
    container = $("#subscribe-box")
    if !container.is(e.target) && container.has(e.target).length == 0
      hideSubscribe()

@showSubscribe = ->
  $("#subscribe-box").show()
  grayOut(true)

@hideSubscribe = ->
  $("#subscribe-box").hide()
  grayOut(false)

@toggleInterestSelect = (interest) ->
  if $( "#"+interest ).hasClass( "selected" )
    $( "#"+interest ).removeClass( "selected" )
  else
    $( "#"+interest ).addClass( "selected" )

@subscribeUser = ->
  # Reset errors
  $("#email").removeClass("error")
  $("#error-email").html("")
  $("#error-interest").html("")

  hasOneSelection = true

  validEmail = false
  if $( "#email" ).val().match emailPattern
    validEmail = true

  if validEmail && hasOneSelection
    url_string = "/subscribe?email=" + $( "#email" ).val() +
      "&design=" + $( "#design" ).hasClass( "selected" ) +
      "&photography=" + $( "#photography" ).hasClass( "selected" ) +
      "&marketing=" + $( "#marketing" ).hasClass( "selected" ) +
      "&development=" + $( "#development" ).hasClass( "selected" )

    $.ajax
      url: url_string
      dataType: "html"
      error: (jqXHR, textStatus, errorThrown) ->
        alert("Sorry, looks like your email is invalid.")
      success: (data, textStatus, jqXHR) ->
        $("#subscribe-form-wrapper").hide()
        $("#subscribe-form-success").show()
        window.setTimeout(hideSubscribe, 4000)
  else
    if !validEmail
      $("#email").addClass("error")
      $("#error-email").html("Invalid email address")
    if !hasOneSelection
      $("#error-interest").html("Select at least one interest")

grayOut = (vis, options) ->
  #  // Pass true to gray out screen, false to ungray
  #  // options are optional. This is a JSON object with the following (optional)
  #  // properties
  #  // opacity:0-100 // Lower number = less grayout higher = more of a blackout
  #  // zindex: # // HTML elements with a higher zindex appear on top of the gray
  #  // out
  #  // bgcolor: (#xxxxxx) // Standard RGB Hex color code
  #  // grayOut(true, {'zindex':'50', 'bgcolor':'#0000FF', 'opacity':'70'});
  #  // Because options is JSON opacity/zindex/bgcolor are all optional and can
  #  // appear
  #  // in any order. Pass only the properties you need to set.
  options = options || {}
  zindex = options.zindex || 50
  opacity = options.opacity || 70
  opaque = (opacity / 100)
  bgcolor = options.bgcolor || '#000000'
  dark = document.getElementById('darkenScreenObject')

  if (!dark)
    #    // The dark layer doesn't exist, it's never been created. So we'll
    #    // create it here and apply some basic styles.
    #    // If you are getting errors in IE see:
    #    // http://support.microsoft.com/default.aspx/kb/927917
    tbody = document.getElementsByTagName("body")[0];
    tnode = document.createElement('div') # Create the layer.
    tnode.style.position = 'absolute' # Position absolutely
    tnode.style.top = '0px' # In the top
    tnode.style.left = '0px' # Left corner of the page
    tnode.style.overflow = 'hidden' # Try to avoid making scroll bars
    tnode.style.display = 'none' # Start out Hidden
    tnode.id = 'darkenScreenObject' # Name it so we can find it later
    tbody.appendChild(tnode) # Add it to the web page
    dark = document.getElementById('darkenScreenObject') # Get the object.

  if (vis)
    pageWidth = '100%'
    pageHeight = '100%'

    #set the shader to cover the entire page and make it visible.
    dark.style.opacity = opaque
    dark.style.MozOpacity = opaque
    dark.style.filter = 'alpha(opacity=' + opacity + ')'
    dark.style.zIndex = zindex
    dark.style.backgroundColor = bgcolor
    dark.style.width = pageWidth
    dark.style.height = pageHeight
    dark.style.display = 'block'
  else
    dark.style.display = 'none'
