class HomeController < ApplicationController
  def index 
  end

  def subscribe
    status = User.subscribe_user(params)
    render json: "status"
  end
end
